<!-- scroll to line  215 :) -->
<?php

session_start();
$user=$_SESSION['Uid']  ;
$dep=$_SESSION['Udep']  ;
$role=$_SESSION['Ur']  ;
$pro=$_SESSION['pro'] ;	

include 'conn.php'  ;

$s=nrole($role, $conn); 
?>

<html>
	<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		</script>	
		
	</head>
	<body style="margin:0.5%">
		
		
		
		<!-- nav-->
		
		<header>
		

	<ul class="nav nav-tabs">
	<?php if($dep==5)
	
   echo '<li class="nav-item">
    <a class="nav-link active" href="tasks.php" id="reports" onclick="taps(this.id)">reports</a>
  </li>  '   ;
  else if($dep==2)
  echo '<li class="nav-item">
    <a class="nav-link active" href="tasks.php" id="it" onclick="taps(this.id)">IT</a>
  </li>'  ; 
  else if($dep==4)
  echo '<li class="nav-item">
    <a class="nav-link active" href="tasks.php" id="hr" onclick="taps(this.id)">HR</a>
  </li>'  ; 
  else  if($dep==3)
    echo '<li class="nav-item">
    <a class="nav-link active" href="tasks.php" id="markting" onclick="taps(this.id)">markting</a>
  </li>'   ; 
  else echo "" ; 
  ?>
  <li class="nav-item">
    <a class="nav-link" href="newpass.php" id="pass" onclick="taps(this.id)">change password</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="LogOutPage.php" id="logout" onclick="taps(this.id)">logout</a>
  </li>
  
</ul>

<div style="float:right"><h6><?php echo $s; ?>: <?php echo $user; ?></h6></div>
<br>

		</header>
<hr>	
<!-- nav end-->
		
<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
  <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#task" role="tab" aria-controls="v-pills-home" aria-selected="true">tasks</a><!-- on click it shows the task table-->
  <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#people" role="tab" aria-controls="v-pills-profile" aria-selected="false">People</a><!-- on click it shows the people table-->

  </div>
  	
<div class="tab-content" id="v-pills-tabContent"><!--div for both tables-->
		
<div>


<!-- Modal: add task -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">ADD TASK</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="addtask.php" method="post">
  <div class="form-group">
    <label for="formGroupExampleInput">name</label>
    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="title" name="name">
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">description</label>
    <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="task..." name="des">
  </div>
  <div class="form-group">
  <div class="row">
  
    <div class="col">
    <label for="formGroupExampleInput2" >due date:</label>

         </div>
    <div class="col">
      <input type="date" class="form-control" name="date">
    </div>
  </div>
  
    </div>
      <button type="submit" class="btn btn-primary">save</button>

</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
    </div>
  </div>
</div>


</div>	
		
	<hr>
		<!--table of task-->
<div class="tab-pane fade show active" id="task" role="tabpanel" aria-labelledby="v-pills-home-tab">
		
		<table class="table table-dark">
  <thead>
    <tr>
	  <th scope="col">	<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModalCenter" >
  ADD
</button></th>
      <th scope="col">name</th>
      <th scope="col">info</th>
      <th scope="col">created by</th>
      <th scope="col">due to</th>
      <th scope="col">status</th>
      <th scope="col"> </th>
    </tr>
  </thead>
  <tbody>
  <?php
  
  /***if(viewtask($pro , $user, $conn, $role ))***/ 
  if(access($user,$dep, $role, 'viewtask',  $conn)){
              $sql="SELECT * FROM tasks WHERE dep_id ='$dep' order by status" ;
              }
              else 
              $sql="SELECT * FROM tasks WHERE dep_id ='$dep' and c_user='$user' order by status" ;
	  
	  if(!$sql)
	  echo 'sorry'   ;
$r=$conn ->query($sql);
 if(!$sql)
	  echo 'sorry'   ;

$i=0  ; 
  while($row=mysqli_fetch_assoc($r) ){
  $i=$i+1;
  echo '<tr>' ;
 echo '<th scope="row">'.$i.'</th>
      <td>'.$row['name'].'</td>
      

<!-- Modal -->
<div class="modal fade" id="'.$row['name'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">'.$row['name'].'</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        '.$row['des'].'
      </div>
      
    </div>
  </div>
</div>
  
     
    <td> <button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#'.$row['name'].'">
  view..</button></td>
  
  
  
  <td>'.$row['c_user'].' @'.$row['posted_t'].'</td>
      <td>'.$row['due_t'].'</td>' ;
      
      
       
      if($row['status']==0)
     echo '<td>	<button type="button" class="btn btn-primary"  onclick="changeto('.$row['id'].' , '.$row['status'].' )">done</button></td>' ;
    else 
    echo '<td><button type="button" class="btn btn-secondary"  onclick="changeto('.$row['id'].' , '.$row['status'].' )">undone</button></td>' ;
    
    if(access($user,$dep, $role, 'deletetask',  $conn)){
     echo '<td><button type="button" class="btn btn-danger"  onclick="delete_t('.$row['id'].')">delete</button></td>' ;
}
else " "  ;
    echo '</tr>'  ; 
    
    
    }

   
    
    
    ?>
  </tbody>
</table>

<hr>
</div>

<!-- hon Ajax!------------------------------------------------------------------------------------------------------------------------------------->
<!-- people div ---------------------------------------------------------------------->

<!-- php files used  here : addpeople.php ; deletepeople.php ;  ---------------------------------------------------------------------->

<div class="tab-pane fade" id="people" role="tabpanel" aria-labelledby="v-pills-profile-tab">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">
      <?php 
      		/**if(addpeople($pro , $user, $conn , $role ))**/
      		     if(access($user,$dep, $role, 'addpeople',  $conn)){
    			 echo  '<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#addpeople">ADD</button>' ;
      }
      		else echo '#'; 
      ?>
      </th>
      <th scope="col">uesername</th>
      <!--<th scope="col">number of tasks</th>-->
      <th scope="col"> </th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
   
  <?php
  
  $i=0  ; 
  $sql2="select * from users where dep_id='$dep' and NOT username='$user'" ; 
  $r2=$conn ->query($sql2);


  while($row2=mysqli_fetch_assoc($r2) ){
 $i=1+$i  ; 
 $_SESSION['userp']=$row2['profile_id']   ;
 
 ?>
    <tr>
    <?php echo '<th scope="row">'.$i.'</th>
      <td>'.$row2['username'].'</td>' ;?>
       <?php if($role==2)
     echo '<td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#editpeople-'.$row2['username'].'" >edit</button></td>';
            else
             echo "<td> </td> " ;  ?>
   <?php /**if(!deletepeople($pro, $user, $conn, $role) || $row2['role_id']==2) echo '' ; else {**/
   if(!access($user,$dep, $role, 'deletepeople',  $conn)) echo '' ; else {  ?>
  <td><a href="deletepeople.php?name=<?php echo ''.$row2['username'].'' ; ?>" >delete</a></td>  
  <?php } ?> 
    
    </tr>    
    
    
    
    <?php   
        
        $editsql='select * from profile where id='.$row2['profile_id'].''  ;
        $r3=$conn ->query($editsql);
        $row3=mysqli_fetch_assoc($r3)  ;

         
        
        ?>
    
   
   <!-- Modal -->
   
   
   
   <!------->
<div class="modal fade" id="editpeople-<?php echo $row2['username'] ; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">edit <?php echo $row2['username']  ; ?> profile</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
       
      <div class="modal-body">
      <form action="edit_m.php" method="post">
		 <input type="hidden" value=<?php echo $row2['profile_id']  ;?>  name="pro"  id="profile">
		 <?php if(employee_access('viewtask', $user, $conn)){
			?>
		  <div class="form-check">        
			<?php 
			if($row3['viewtask']==0)
			  echo '<input class="form-check-input" type="checkbox" value="vt"  name="CH[]" id="defaultCheck1"> '  ; 
			 else
			  echo '<input class="form-check-input" type="checkbox" value="vt"  name="CH[]" id="defaultCheck1" checked> '  ;
			   
			?>
		  <label class="form-check-label" for = "defaultCheck1">
			view tasks
		  </label>
		</div>
		<?php  }
		else echo ""  ; ?> 

		 <?php if(employee_access('deletetask', $user, $conn)){
			?>
		  
        <div class="form-check">        
			<?php 
			if($row3['deletetask']==0)
			  echo '<input class="form-check-input" type="checkbox" value="dt"  name="CH[]" id="defaultCheck2"> '  ; 
			 else
			  echo '<input class="form-check-input" type="checkbox" value="dt"  name="CH[]" id="defaultCheck2" checked> '  ; 
			?>
		  <label class="form-check-label" for = "defaultCheck2">
			delete task
		  </label>
		</div>
		<?php } else ""  ;  ?>
		
		<?php if(employee_access('addpeople', $user, $conn)){
			?>
		 
		<div class="form-check">
			<?php 
			if($row3['addpeople']==0)
			  echo '<input class="form-check-input" type="checkbox" value="ap" name="CH[]" id="defaultCheck3">'  ; 
			 else 
			  echo '<input class="form-check-input" type="checkbox" value="ap" name="CH[]" id="defaultCheck3" checked>'  ;
		  ?>
		  <label class="form-check-label" for="defaultCheck3">
			add people
		  </label>
		</div>
		<?php } else ""  ;?>
		
<?php if(employee_access('deletepeople', $user, $conn)){
			?>
		  
		<div class="form-check">
			<?php
			if($row3['deletepeople']==0)
			  echo '<input class="form-check-input" type="checkbox" value="dp" name="CH[]" id="defaultCheck4">'  ; 
			 else 
			  echo '<input class="form-check-input" type="checkbox" value="dp" name="CH[]" id="defaultCheck4" checked>'  ;
			  
			 ?>
		  <label class="form-check-label" for="defaultCheck4">
			 delete people
		  </label>
		</div>
		<?php } echo ""  ;?>  
		
      <button type="submit" class="btn btn-primary">save</button> 
      </form>
	  </div> <!--  closing body-->
	  </div> <!--  closing content-->
    </div> <!--  closing dialog-->
 </div><!--   closing modaal-->


    
    
    
    
    <?php 
    
       }
	 ?>

</tbody>
</table>
</div>

<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="addpeople" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">add employee to <?php echo ndep($dep, $conn)?> department (username should be unique)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="addpeople.php" >
  <div class="form-row align-items-center">
      <h3>username</h3>
            <input type="text" class="form-control" id="us1" placeholder="jonsmith\jon_smith" name="user" required>
        <br>
  <h3>password:</h3>
  <input type="password" class="form-control" id="p1"  name="pass" required>
  </div>
  <br>
  <h3>user accesses:</h3>
  <div class="form-check">
  <input class="form-check-input" type="checkbox" value="vt"  name="vt" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
    view task
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="dt"  name="dt" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
    delete task
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="ap" name="ap" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
    add people
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="checkbox" value="dp" name="dp" id="defaultCheck1">
  <label class="form-check-label" for="defaultCheck1">
     delete people
  </label>
</div>
  
 <button type="submit" class="btn btn-primary mb-2" style="margin:8px" >Submit</button>
  
 
</form>
      </div>
      
    </div>
  </div>
</div><!--- people div ends---->

	</div>
	
	<!--hon ma shi bas script-->

     <script>
                     
function changeto(id, s)
                     
                     
{
    if(s==1)
        alert("task is undone!")  ;
    
    else
        alert("task is done!")  ;

     
    window.location.href = "status.php?id="+id+"&status="+s;
     
 }
 
 
 
function delete_t(id)
{
 var result1= confirm("Are you sure you want to delete this task? ");
    if(result1){
           window.location.href = "deletepost.php?name="+id;
    
        
        alert("task is deleted!")  ;
        }  
}

function taps(id)
{
document.getElementById(id).className = "nav-link active";
}





			
	</script>		
			
	</body>
</html>
