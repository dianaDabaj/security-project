<?php
include("connect.php");
//include("/controller/common.php");
//include 'adminAction.php';
session_start();

$logged_in = isset($_SESSION["logged_user"]) ?  $_SESSION["logged_user"] : ''  ;
 
if(empty($logged_in)){
    header('Location: index.php');
}
else{
$sql ="SELECT * FROM user";
$result = $conn->query($sql);

if ($result) {

    ?>
    <html>
        
        <head>
            <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
            <link id="pagestyle" rel="stylesheet" type="text/css" href="mystyle.css">
            <script language="JavaScript" type="text/javascript" src="js/jquery.min.js"></script>
            <script async="" src="js/script.js"></script>
            <script>
function show_confirm22()
    {
    var r=confirm("The entry will be parmanently deleted!");
    if (r==true)
    {return true;
    }
    else
    {
    return false;
    }
    }
</script>
        </head>


 
<body class="admin-page">
    <div id="mySidenav" class="sidenav">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <a href="welcome.php">About</a>
            <a href="edit.php">Profile</a>
            <?php  if ($_SESSION["logged_user"]["role"]=="Admin")
            {
            ?>
                 <a href="admin.php">Administrator</a>
                 <a href="product.php">Products</a>
            <?php 
            }?>

</div>
    
    
     
      <span style="font-size:30px;cursor:pointer; float: left;margin-left: 40px;" onclick="openNav()">&#9776; </span>
    <div class="loader-form">
        <form method="post" action="deleteMultipleData.php" style="margin-left: 6%;">

        <table width='80%' border=0 id="tableUser">
        <tr bgcolor='#CCCCCC'>
            <td></td>
            <td>ID</td>
            <td>Username</td>
            <td>First Name</td>
            <td>Last Name</td>
            <td>Nationality</td>
            <td>Gender</td>
            <td>Email</td>
            <td>Role</td>
            <td style="padding-right: 100px;">Update</td>
        </tr>
        <?php 
        foreach ($userInfoArray as $userInfo) { ?>
        <tr data-userid='<?php echo $userInfo['user_id'] ?>'>
            <td class="center">
                <span>
                    <input type="checkbox" name="box[]" class="case" value="<?php echo $userInfo['user_id'] ?>">
                </span>
                </td>
            <td><?php echo $userInfo['user_id'] ?></td>
            <td><input class="typing username" type="text" style="border: none;" value="<?php echo $userInfo['username'] ?>" readonly></td>
            <td><input class="typing firstname" type="text" style="border: none;" value="<?php echo $userInfo['firstname'] ?>" readonly></td>
            <td><input class="typing lastname" type="text" style="border: none;" value="<?php echo $userInfo['lastname'] ?>" readonly></td>
            <td><input class="typing nationality" type="text" style="border: none;" value="<?php echo $userInfo['nationality'] ?>" readonly></td>
            <td><input class="typing gender" type="text" style="border: none;" value="<?php echo $userInfo['gender'] ?>" readonly></td>
            <td><input class="typing email" type="text" style="border: none;" value="<?php echo $userInfo['email'] ?>" readonly></td>
               <input type="hidden" id="OLD_VAL_EMAIL_<?php echo $userInfo['user_id'] ?>" value="<?php echo  $userInfo['email']?>"/>
            <td><input class="typing role" type="text" style="border: none;" value="<?php echo $userInfo['role'] ?>" readonly></td>
            <td style="width: 400px;"><a class="edit-user">Edit|</a><a class="delete-user">Delete</a><a class="save-user">|Save|</a><a class="reset-user">Reset</a></td>
        </tr>
        
        <?php
        }
        ?>
        
    </table>

    <div style="margin-top:20px;">
                         <a href="add.html" style="color: white;text-decoration: none;background-color: red;float: left;padding: 0.5em;border-radius: 5px;display: inline-block;">Add New Data</a><br/><br/>

        <div style="clear: both"></div>
         <div style=" float:left;">
           &nbsp;<input type="checkbox" id="selectall"/> Check All
           <input name="delete" type="submit" id="delete" value="Delete" onClick="return show_confirm22();" style="background:#F52126; color:#fff; border:none; font-size:12px; padding:4px 8px; border-radius: 3px; display: inline-block">
         
         </div>
         <div style="clear:both;"></div>
    </div>

        </form>

        </div>
    <div style="float: right">
        <a href="Welcome.php" style="color: white">Back</a>
    </div>
</body>

</html>
<?php
} else {
    $response = 0;
    echo $response;
    return $response;
}

}?>
 

